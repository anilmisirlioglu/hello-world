package sum

import "math"

var Delta int = math.MaxInt64

type X struct{}

func Sum(x int, y int) int {
	return x + y
}

func Extract(x int, y int) int {
	return x - y
}

func String() string {
	return "Hello World"
}
